from DoG import DoG
import cv2 
import matplotlib.pyplot as plt
#%matplotlib inline
#reading image
# read images
img1 = cv2.imread('eiffel_1.png')  
img1 = cv2.imread('eiffel_1.png') 

#img1 = DoG(img1)
#img2 = DoG(img2)

gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

#keypoints
sift = cv2.xfeatures2d.SIFT_create()
keypoints_1, descriptors_1 = sift.detectAndCompute(img1,None)

img_1 = cv2.drawKeypoints(gray1,keypoints_1,img1)
plt.imshow(img_1)
plt.show()