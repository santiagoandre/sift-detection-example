import cv2
import numpy as np
import matplotlib.pyplot as plt

def DoG(img):
    #fn = raw_input("Enter image file name and path: ")
    #fn_no_ext = fn.split('.')[0]
    #outputFile = fn_no_ext+'.png'
    #read the input file
#    img = cv2.imread(str(fn))

    #run a 5x5 gaussian blur then a 3x3 gaussian blr
    blur3 = cv2.GaussianBlur(img,(7, 7),0)
    blur5 = cv2.GaussianBlur(img,(9,9),0)

    #write the results of the previous step to new files
    plt.imshow(blur5),plt.show()
    #cv2.imwrite(fn_no_ext+'3x3.jpg', blur3)
    #cv2.imwrite(fn_no_ext+'5x5.jpg', blur5)

    DoGim = blur5 - blur3
    return DoGim
    #plt.imshow(DoGim),plt.show()
    #cv2.imwrite(outputFile, DoGim)
 
img1 = cv2.imread('../example2/res/obama1.jpg')  
DoGim =  DoG(img1)
plt.imshow(DoGim),plt.show()